/*---------------------------------------------------------------------------------------------------------*/
/*                                                                                                         */
/* Copyright (c) Nuvoton Technology Corp. All rights reserved.                                             */
/*                                                                                                         */
/*---------------------------------------------------------------------------------------------------------*/

#include <stdio.h>
#include "NUC1xx.h"
#include "Driver\DrvSYS.h"
#include "Driver\DrvGPIO.h"
#include "LCD_Driver.h"
#include "EEPROM_24LC64.h"
#include "Driver\DrvI2C.h"
#include "Seven_Segment.h"
void delay_loop(void)
 {
 uint32_t i,j;
	for(i=0;i<3;i++)	
	{
		for(j=0;j<600;j++);
    }
 
 }
 
 void cleanrow(char row[9]){
			row[0]=' ';
			row[1]=' ';
			row[2]=' ';
			row[3]=' ';
			row[4]=' ';
			row[5]=' ';
			row[6]=' ';
			row[7]=' ';
			row[8]=' ';
}

char change(int a)
{
	if (a==0) {return '0';}
	if (a==1) {return '1';}
	if (a==2) {return '2';}
	if (a==3) {return '3';}
	if (a==4) {return '4';}
	if (a==5) {return '5';}
	if (a==6) {return '6';}
	if (a==7) {return '7';}
	if (a==8) {return '8';}
	if (a==9) {return '9';}
	if (a==10) {return 'A';}
	if (a==11) {return 'B';}
	if (a==12) {return 'C';}
	if (a==13) {return 'D';}
	if (a==14) {return 'E';}
	if (a==15) {return 'F';}
	if (a==16) {return ' ';}
	if (a==17) {return 'H';}
}

void delay(void)
{
int j;
   for(j=0; j<1000; j++);
}

uint8_t scan_key(void)
{
uint8_t act[4]={0x3b, 0x3d, 0x3e};    
uint8_t i,temp,pin;

for(i=0;i<3;i++)
{
       temp=act[i];
	   for(pin=0;pin<6;pin++)
	    {
	  	if((temp&0x01)==0x01)
        DrvGPIO_SetBit(E_GPA,pin);
		else
		DrvGPIO_ClrBit(E_GPA,pin);		  
		temp=temp>>1; 	  
	    }							    
	    delay();
		if(DrvGPIO_GetBit(E_GPA,3)==0) 
		return (i+1);
}

for(i=0;i<3;i++)
{
       temp=act[i];
	   for(pin=0;pin<6;pin++)
	    {
	  	if((temp&0x01)==0x01)
        DrvGPIO_SetBit(E_GPA,pin);
		else
		DrvGPIO_ClrBit(E_GPA,pin);		  
		temp=temp>>1; 	  
	    }							    
	    delay();
		if(DrvGPIO_GetBit(E_GPA,4)==0) 
		return (i+4);
}

for(i=0;i<3;i++)
{
       temp=act[i];
	   for(pin=0;pin<6;pin++)
	    {
	  	if((temp&0x01)==0x01)
        DrvGPIO_SetBit(E_GPA,pin);
		else
		DrvGPIO_ClrBit(E_GPA,pin);		  
		temp=temp>>1; 	  
	    }							    
	    delay();
		if(DrvGPIO_GetBit(E_GPA,5)==0) 
		return (i+7);
}

 return 0;

}



int main(void)
{
	int x=0,y=0;
	char seg_array[16][4]= "";
	int j=0,k=0,l=0,m=16,n=16,o=16,p=16,segar=0;
	char row[9] = "";
	  
	uint32_t i2cdata=0,i;
	unsigned char temp=0;
	unsigned char last=0;
	char addr[16]="Address:";
	char Write[16]="Write:";
	char read[16]="Read:";
	
	int t1=0;
	int t2=0;
	int t21=0;
	int t3=0;
	int t31=0;
	int t4=0;
	int t5=0;
	int t51=0;
	int t6=0;
	int t61=0;
	int t9=0;
	
	/* Unlock the protected registers */	
	UNLOCKREG();
   	/* Enable the 12MHz oscillator oscillation */
	DrvSYS_SetOscCtrl(E_SYS_XTL12M, 1);
 
     /* Waiting for 12M Xtal stalble */
    SysTimerDelay(5000);
 
	/* HCLK clock source. 0: external 12MHz; 4:internal 22MHz RC oscillator */
	DrvSYS_SelectHCLKSource(0);		
    /*lock the protected registers */
	LOCKREG();				

	DrvSYS_SetClockDivider(E_SYS_HCLK_DIV, 0); /* HCLK clock frequency = HCLK clock source / (HCLK_N + 1) */

	Initial_pannel();  //call initial pannel function
	clr_all_pannal();

	
	
	print_lcd(0, "I2C with 24LC65");
	print_lcd(1, "test read and  ");
	print_lcd(2, "write function ");	  
	print_lcd(3, "press key1-key9");
	//initial key board
	for(i=0;i<6;i++)		
	DrvGPIO_Open(E_GPA, i, E_IO_QUASI);



	while(1)
	{
		/*
		while (temp==0)
		{
			for(x=0;x<9000;x++)
			{
				for(y=0;y<4;y++)
				{
					close_seven_segment();
					show_seven_segment(y,16);
					delay_loop();
					temp=scan_key();
					if (temp!=0) break;
				}
				if (temp!=0) break;
			}   		
		} */
		
		
		for(j=0;j<10000;j++){				
					for(k=0;k<100;k++){	
							close_seven_segment();
							show_seven_segment(0,m);
							delay_loop();
							close_seven_segment();
							show_seven_segment(1,n);
							delay_loop();
							close_seven_segment();
							show_seven_segment(2,o);
							delay_loop();
							close_seven_segment();
							show_seven_segment(3,p);
							delay_loop(); 
	   	
		if(temp==1)
		{
			print_lcd(0,"Key1 had pressed ");
		  print_lcd(1,"               ");
		  print_lcd(2,"               ");
			print_lcd(3,"               ");
			
		  DrvGPIO_InitFunction(E_FUNC_I2C1);
			Write_24LC64(0x00000000+temp,temp+00);
			i2cdata= Read_24LC64(0x00000000+temp);
		  
			sprintf(addr+8,"%x",temp);
			sprintf(Write+6,"%x",temp+11);
			sprintf(read+5,"%x",i2cdata);
			
			
			if (last != 1) {
				p=o;
				o=n;
				n=m;
				m=l;
			}
			
					if (t1 == 0) {
						l=0;
						m=0;
						t1 = 1;
					}else if (t1 == 1) {
						l=1;
						m=1;
						t1 = 0;
					}
					
			last=1;
		}
		if(temp==2)
		{ 
			print_lcd(0,"Key2 had pressed ");
		  print_lcd(1,"               ");
		  print_lcd(2,"               ");
			print_lcd(3,"               ");
			
			DrvGPIO_InitFunction(E_FUNC_I2C1);
			Write_24LC64(0x00000000+temp,temp+22);
			i2cdata= Read_24LC64(0x00000000+temp);
			
		  sprintf(addr+8,"%x",temp);
			sprintf(Write+6,"%x",temp+22);
			sprintf(read+5,"%x",i2cdata);
			
			
			if (last != 2) {
					p=o;
					o=n;
					n=m;
					m=l;
				}
			
				if (t2 == 0) {
					l=2;
					m=2;
					t2 = 1;
				}else if ((t2 == 1) && (t21 == 0)) {
					l=3;
					m=3;
					t21  = 1;
				}else if ((t2 == 1) && (t21 == 1)) {
					l=4;
					m=4;
					t2 = 0;
					t21 = 0;
				}
				
				last = 2;
		}
		if(temp==3)
		{
			print_lcd(0,"Key3 had pressed ");
		  print_lcd(1,"               ");
		  print_lcd(2,"               ");
			
			DrvGPIO_InitFunction(E_FUNC_I2C1); 
			Write_24LC64(0x00000000+temp,temp+33);
			i2cdata= Read_24LC64(0x00000000+temp);
		  
			sprintf(addr+8,"%x",temp);
			sprintf(Write+6,"%x",temp+33);
			sprintf(read+5,"%x",i2cdata);
			
			
				
			if (last != 3) {	
				p=o;
				o=n;
				n=m;
				m=l;
			}
			
				if (t3 == 0) {
					l=5;
					m=5;
					t3 = 1;
				}else if ((t3 == 1) && (t31 == 0)) {
					l=6;
					m=6;
					t31  = 1;
				}else if ((t3 == 1) && (t31 == 1)) {
					l=7;
					m=7;
					t3 = 0;
					t31 = 0;
				}
			
			last = 3;
		}
		if(temp==4)
		{
			print_lcd(0,"Key4 had pressed ");
			print_lcd(1,"               ");
		  print_lcd(2,"               ");
			
			DrvGPIO_InitFunction(E_FUNC_I2C1);
			Write_24LC64(0x00000000+temp,temp+44);
			i2cdata= Read_24LC64(0x00000000+temp);
		  
			sprintf(addr+8,"%x",temp);
			sprintf(Write+6,"%x",temp+44);
			sprintf(read+5,"%x",i2cdata);
			
			
			
					if (last != 4) {
						p=o;
						o=n;
						n=m;
						m=l;
					}
			
					if (t4 == 0) {
						l=8;
						m=8;
						t4 = 1;
					}else if (t4 == 1) {
						l=9;
						m=9;
						t4 = 0;
					}
					
					last = 4;
		}
		if(temp==5)
		{
			print_lcd(0,"Key5 had pressed ");
		  print_lcd(1,"               ");
		  print_lcd(2,"               ");	
			
			DrvGPIO_InitFunction(E_FUNC_I2C1);
			Write_24LC64(0x00000000+temp,temp+55);
			i2cdata= Read_24LC64(0x00000000+temp);
		  
			sprintf(addr+8,"%x",temp);
			sprintf(Write+6,"%x",temp+55);
			sprintf(read+5,"%x",i2cdata);
			
				if (last != 5) {	
					p=o;
					o=n;
					n=m;
					m=l;
				}
			
				if (t5 == 0) {
					l=10;
					m=10;
					t5 = 1;
				}else if ((t5 == 1) && (t51 == 0)) {
					l=11;
					m=11;
					t51  = 1;
				}else if ((t5 == 1) && (t51 == 1)) {
					l=12;
					m=12;
					t5 = 0;
					t51 = 0;
				}
			
				last = 5;
		}
		if(temp==6)
		{
			print_lcd(0,"Key6 had pressed ");
		  print_lcd(1,"               ");
		  print_lcd(2,"               ");
			
			DrvGPIO_InitFunction(E_FUNC_I2C1);
			Write_24LC64(0x00000000+temp,temp+66);
			i2cdata= Read_24LC64(0x00000000+temp);
			
		  sprintf(addr+8,"%x",temp);
			sprintf(Write+6,"%x",temp+66);
			sprintf(read+5,"%x",i2cdata);
				
			if (last != 6) {	
					p=o;
					o=n;
					n=m;
					m=l;
				}
			
				if (t6 == 0) {
					l=13;
					m=13;
					t6 = 1;
				}else if ((t6 == 1) && (t61 == 0)) {
					l=14;
					m=14;
					t61  = 1;
				}else if ((t6 == 1) && (t61 == 1)) {
					l=15;
					m=15;
					t6 = 0;
					t61 = 0;
				}
				last = 6;
	    }
		if(temp==7)
		{
			print_lcd(0,"Key7 had pressed ");
		  print_lcd(1,"               ");
			print_lcd(2,"               ");
			
			DrvGPIO_InitFunction(E_FUNC_I2C1);  	  	  
		 	Write_24LC64(0x00000000+temp,temp+77);
			i2cdata= Read_24LC64(0x00000000+temp);
		  
			sprintf(addr+8,"%x",temp);
			sprintf(Write+6,"%x",temp+77);
			sprintf(read+5,"%x",i2cdata);
			
			m=n;
			n=o;
			o=p;
			p=16;
		}
		if(temp==8)
		{
				clr_all_pannal();
				seg_array[segar][0]=change(p);
				seg_array[segar][1]=change(o);
				seg_array[segar][2]=change(n);
				seg_array[segar][3]=change(m);
				print_lcd(0,"This is enter  ");
		    print_lcd(1, "              ");
		    print_lcd(2,"               ");
			
			DrvGPIO_InitFunction(E_FUNC_I2C1);  	  	  
		 	Write_24LC64(0x00000000+temp,temp+88);
			i2cdata= Read_24LC64(0x00000000+temp);
		  
			sprintf(addr+8,"%x",temp);
			sprintf(Write+6,"%x",temp+88);
			sprintf(read+5,"%x",i2cdata);
			
			row[0]=seg_array[0][0];
			row[1]=seg_array[0][1];
			row[2]=seg_array[0][2];
			row[3]=seg_array[0][3];
			row[4]=' ';
			if(seg_array[1][0]!=NULL){			
			row[5]=seg_array[1][0];
			row[6]=seg_array[1][1];
			row[7]=seg_array[1][2];
			row[8]=seg_array[1][3];
			}
			//insrow1(row,seg_array);
			print_lcd(1,row);
			//insrow2(row,seg_array);
			cleanrow(row);
			
			if(seg_array[2][0]!=NULL){			
			row[0]=seg_array[2][0];
			row[1]=seg_array[2][1];
			row[2]=seg_array[2][2];
			row[3]=seg_array[2][3];
			row[4]=' ';
			
			if(seg_array[3][0]!=NULL){
			row[5]=seg_array[3][0];
			row[6]=seg_array[3][1];
			row[7]=seg_array[3][2];
			row[8]=seg_array[3][3];
		}
			print_lcd(2,row);	
		}
			if(segar<16){segar++;}		
		}
		if(temp==9)
		{
			print_lcd(0,"Key9 had pressed");
		  print_lcd(1,"               ");
		  print_lcd(2,"               ");
			
			DrvGPIO_InitFunction(E_FUNC_I2C1);  	  	  
		 	Write_24LC64(0x00000000+temp,temp+99);
			i2cdata= Read_24LC64(0x00000000+temp);
		  sprintf(addr+8,"%x",temp);
			sprintf(Write+6,"%x",temp+99);
			sprintf(read+5,"%x",i2cdata);
			
			clr_all_pannal();
			//if(seg_array[9][1]!=NULL) {print_lcd(3,seg_array[9]);}
						
			////////////
			if (t9 == 0) {
					t9 = 1;
				
						//insrow3(row,seg_array);
						cleanrow(row);
						if(seg_array[4][0]!=NULL){						
						row[0]=seg_array[4][0];
						row[1]=seg_array[4][1];
						row[2]=seg_array[4][2];
						row[3]=seg_array[4][3];
						row[4]=' ';
						if(seg_array[5][0]!=NULL){						
						row[5]=seg_array[5][0];
						row[6]=seg_array[5][1];
						row[7]=seg_array[5][2];
						row[8]=seg_array[5][3];
						}
						print_lcd(0,row);
					}
						//insrow4(row,seg_array);
						cleanrow(row);
						if(seg_array[6][0]!=NULL){			
						row[0]=seg_array[6][0];
						row[1]=seg_array[6][1];
						row[2]=seg_array[6][2];
						row[3]=seg_array[6][3];
						row[4]=' ';
						if(seg_array[7][0]!=NULL){			
						row[5]=seg_array[7][0];
						row[6]=seg_array[7][1];
						row[7]=seg_array[7][2];
						row[8]=seg_array[7][3];
						}
						print_lcd(1,row);
						}//insrow5(row,seg_array);
						cleanrow(row);
						if(seg_array[8][0]!=NULL){			
						row[0]=seg_array[8][0];
						row[1]=seg_array[8][1];
						row[2]=seg_array[8][2];
						row[3]=seg_array[8][3];
						row[4]=' ';	
						if(seg_array[9][0]!=NULL){			
						row[5]=seg_array[9][0];
						row[6]=seg_array[9][1];
						row[7]=seg_array[9][2];
						row[8]=seg_array[9][3];
						}
						print_lcd(2,row);				
						}
						}else if (t9 == 1){	
							/////////////////
						t9 = 0;
						//insrow3(row,seg_array);
						cleanrow(row);
						if(seg_array[10][0]!=NULL){						
						row[0]=seg_array[10][0];
						row[1]=seg_array[10][1];
						row[2]=seg_array[10][2];
						row[3]=seg_array[10][3];
						row[4]=' ';
						if(seg_array[11][0]!=NULL){						
						row[5]=seg_array[11][0];
						row[6]=seg_array[11][1];
						row[7]=seg_array[11][2];
						row[8]=seg_array[11][3];
						}
						print_lcd(0,row);
					}
						//insrow4(row,seg_array);
						cleanrow(row);
						if(seg_array[12][0]!=NULL){			
						row[0]=seg_array[12][0];
						row[1]=seg_array[12][1];
						row[2]=seg_array[12][2];
						row[3]=seg_array[12][3];
						row[4]=' ';
						if(seg_array[13][0]!=NULL){			
						row[5]=seg_array[13][0];
						row[6]=seg_array[13][1];
						row[7]=seg_array[13][2];
						row[8]=seg_array[13][3];
						}
						print_lcd(1,row);
						}//insrow5(row,seg_array);
						cleanrow(row);
						if(seg_array[14][0]!=NULL){			
						row[0]=seg_array[14][0];
						row[1]=seg_array[14][1];
						row[2]=seg_array[14][2];
						row[3]=seg_array[14][3];
						row[4]=' ';
						if(seg_array[15][0]!=NULL){			
						row[5]=seg_array[15][0];
						row[6]=seg_array[15][1];
						row[7]=seg_array[15][2];
						row[8]=seg_array[15][3];
						}
						print_lcd(2,row);				
						}

					}
					last = 9;	

			
	    }
		temp=scan_key();
		}
		
		}
		
	}


	  		
}


